module.exports = async function (browser, event) {
    console.log("event", event);

    // include node fs module
    var fs = require('fs');

    fs.writeFile(process.env.ARTIFACTS_PATH + '/ARTIFACT-1.png', '1', function (err) {
        if (err) throw err;
        console.log('artifact is created successfully...');
    });

    fs.writeFile(process.env.SCREENSHOTS_PATH + '/SCREENSHOT-1.png', '2', function (err) {
        if (err) throw err;
        console.log('screenshot is created successfully.');
    });

    throw new Error("Something went wrong! Attachments should be stored...")

    return { created: 'ok', taskTitle: "https://bbbb.com" };
};
