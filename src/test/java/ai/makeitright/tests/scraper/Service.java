package ai.makeitright.tests.scraper;

import ai.makeitright.utilities.DriverConfig;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.junit.Before;
import org.junit.Test;

import java.util.Map;


public class Service extends DriverConfig {

    Gson gson = new GsonBuilder().setPrettyPrinting().create();

    private String BaseURL;
    private String City;
    private String State;

    @Before
    public void before() {
        BaseURL = System.getProperty("inputParameters.baseURL");
        City = System.getProperty("inputParameters.city").toLowerCase();
        State = System.getProperty("inputParameters.state").toLowerCase();
    }

    @Test
    public void getRealtors() {
        System.out.println("+1");

        System.setProperty("output", "{\"result\": {\"aaa\": \"val1\"}}");

        System.out.println("output:" + gson.fromJson(System.getProperty("output"), Map.class));
    }

}
