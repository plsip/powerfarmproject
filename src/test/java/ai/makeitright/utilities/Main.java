package ai.makeitright.utilities;

import org.junit.After;
import org.junit.Before;

public class Main {
    public static Reporter report;
    public static String workspacePath = System.getProperty("ARTIFACTS_PATH");
    public final static String reportName = "Report.html";
    public static String serviceName = "";

    @Before
    public void tearUp() {
        System.out.println("+4 staging");
        report = new Reporter(workspacePath + System.getProperty("file.separator") + reportName);
        report.startTest(serviceName);
    }

    @After
    public void afterMethod() {
        report.closeRaport();
    }
}
